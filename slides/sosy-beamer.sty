\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesPackage{sosy-beamer}[2018/05/04 Package for presentations of SoSy-Lab]

\usepackage{etoolbox}

%%%%%%%%%% STYLE %%%%%%%%%%

\ifcsname setbeamercolor\endcsname
  \setbeamercolor{structure}{fg=sosyblue}

  \setbeamercolor{block title}{bg=unigrey!20}
  \setbeamercolor{block body}{bg=unigrey!20}

  \setbeamertemplate{navigation symbols}{}
  \usefonttheme[onlymath]{serif}

  \setbeamertemplate{footline}{
    \leavevmode%
    \hbox{%
    \begin{beamercolorbox}[wd=.333333\paperwidth,ht=0.25ex,dp=1ex,center]{author in head/foot}%
      \color{unigrey}
      \usebeamerfont{author in head/foot}\insertshortauthor
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.333333\paperwidth,ht=0.25ex,dp=1ex,center]{institute in head/foot}%
      \color{unigrey}
      \usebeamerfont{title in head/foot}\insertshortinstitute
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.333333\paperwidth,ht=0.25ex,dp=1ex,right]{frame number in head/foot}%
      \color{unigrey}
      \insertframenumber{} / \insertpresentationendframe\hspace*{2ex}
    \end{beamercolorbox}}%
   }

  % Provide \insertpresentationendframe for the last frame before the appendix
  % (similar to \inserpresentationendpage from beamer)
  \providecommand\insertpresentationendframe\inserttotalframenumber
  \appto\appendix{%
    \immediate\write\@mainaux{\string\gdef\string\insertpresentationendframe{\insertframenumber}}%
  }
\fi

\RequirePackage{lmodern}
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage{microtype}

% GENERAL
\RequirePackage{calc}
\RequirePackage{expl3}
\RequirePackage{fixltx2e}
\RequirePackage{xspace}
\RequirePackage{relsize} % for \smaller
\RequirePackage{comment} % for commenting out blocks


\RequirePackage[overlay]{textpos}
\setlength\TPHorizModule{1cm}
\setlength\TPVertModule{1cm}

%%%%%%%%%% COLORS %%%%%%%%%%
\RequirePackage{xcolor}
\newcommand\definergbcolor[2]{\definecolor{#1}{rgb}{#2}}
\newcommand\definecmykcolor[2]{\definecolor{#1}{cmyk}{#2}}

% Blue of the 'C' in the CPAchecker logo and the background of the SoSy-Lab logo
\definergbcolor{sosyblue}{0.3058823529411765 0.615686274509804 0.8392156862745098}
% Green of the checkmark in the CPAchecker logo
\definergbcolor{green}{0 0.63529414 0}
% Uni Passau colors, still needed for the 'P' and the 'A' in the CPAchecker logo
\definecmykcolor{uniorange}{0 0.5 1 0}
\definecmykcolor{unigrey}{0.08 0 0.06 0.47}
% Green of lmu logo
\definergbcolor{lmugreen}{0.1843137254901961 0.5215686274509804 0.2980392156862745}

%%%%%%%%%%%%% MATH %%%%%%%%%%
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{stackrel}
% Declare \bigtimes symbol copied from mathabx package, which should not be included together with amssymb due to incompatibilities:
% https://tex.stackexchange.com/questions/28150/how-can-i-get-a-big-cross-to-denote-a-generalized-cartesian-product
% https://tex.stackexchange.com/questions/4955/how-to-use-bigtimes-without-pain/4960#4960
  \DeclareFontFamily{U}{mathx}{\hyphenchar\font45}
  \DeclareFontShape{U}{mathx}{m}{n}{
        <5> <6> <7> <8> <9> <10>
        <10.95> <12> <14.4> <17.28> <20.74> <24.88>
        mathx10
        }{}
  \DeclareSymbolFont{mathx}{U}{mathx}{m}{n}
  \DeclareMathSymbol{\bigtimes}{1}{mathx}{"91}
% End declaration of \bigtimes symbol


%%%%%%%%%%%%% TEXT %%%%%%%%%%
% SI-Units
\RequirePackage[group-digits=integer, group-minimum-digits=4,
                list-final-separator={, and }, add-integer-zero=false,
                free-standing-units, unit-optional-argument, % easier input of numbers with units
                binary-units,
                detect-weight=true,detect-inline-weight=math, % for bold cells
                per-mode=fraction, % we use fractions for units instead of negative exponents
                ]{siunitx}
\newcommand\GB[1]{\SI{#1}{\giga\byte}}
\newcommand\GHz[1]{\SI{#1}{\giga\hertz}}
\RequirePackage{fontawesome}


%%%%%%%%%%%%% TABLES %%%%%%%%%%
\RequirePackage{longtable}
\RequirePackage{afterpage} % for placing longtables like floats
\RequirePackage{booktabs}
\RequirePackage{multirow}
\RequirePackage{rotating} % for rotated cells

%%%%%%%%%% FIGURES %%%%%%%%%%
% GnuPlot generates PDF 1.5, whereas LaTeX by default generates PDF 1.4
% and produces a warning about the figures.
% This increments the version of the resulting PDF to 1.5
% such that included figures in PDF 1.5 are legal.
\pdfminorversion=5

\RequirePackage{graphicx}

\usepackage[overlay]{textpos}
\setlength\TPHorizModule{1cm}
\setlength\TPVertModule{1cm}

\RequirePackage{pgfplots} % for plots together with tikz
\RequirePackage{pgfplotstable}
\pgfplotsset{compat=1.13,
             log ticks with fixed point, % no scientific notation in plots
             table/col sep=tab, % only tabs are column separators
             unbounded coords=jump, % better have skips in a plot than appear to be interpolating
             filter discard warning=false, % Don't complain about empty cells
             }
\SendSettingsToPgf % use siunitx formatting settings in PGF, too
% Define new pgfmath function for the logarithm to base 10 that also works with fpu library
\pgfmathdeclarefunction{lg10}{1}{\pgfmathparse{ln(#1)/ln(10)}}

% Allows inclusion of figures from separate files
% that can also be compiled on their own:
\RequirePackage{standalone}

% TikZ
\RequirePackage{tikz,pgf}
\usetikzlibrary{calc}
\usetikzlibrary{shapes}
\usetikzlibrary{arrows}
\usetikzlibrary{chains}
\usetikzlibrary{positioning}
\usetikzlibrary{fit}
\usetikzlibrary{automata}

\newcommand\tikzmark[1]{%
  \tikz[remember picture,overlay]\node (#1) {};%
}

% Allows to use only=<overlay{} and alt={<overlay>{then}{else}} in tikz styles
\tikzset{
  only/.code args={<#1>#2}{
    \only<#1>{\pgfkeysalso{#2}}
  },
  alt/.code args={<#1>#2#3}{
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}}
  },
}

\tikzset{
  abstraction state/.style={
    fill=green!60,
    rounded corners=3pt,
    inner sep=3pt,
  },
  covering edge/.style={
    draw,
    ->,
    dashed,
  },
  arg edge/.style={
    draw,
    ->,
  },
  multi arg edge/.style={
    arg edge,
    decorate, decoration={snake, amplitude=.5pt, segment length=6pt,},
    -LaTeX,
  },
  currentstate/.style={
    fill=black!20,
  },
}

\makeatletter
\long\def\ifnodedefined#1#2#3{%
    \@ifundefined{pgf@sh@ns@#1}{#3}{#2}%
}
\newcommand\aeundefinenode[1]{%%
  \expandafter\ifx\csname pgf@sh@ns@#1\endcsname\relax
  \else
    \typeout{===>Undefining node "#1"}%%
    \global\expandafter\let\csname pgf@sh@ns@#1\endcsname\relax
  \fi
}
\newcommand*\ifcounter[1]{%
  \ifcsname c@#1\endcsname
    \expandafter\@firstoftwo
  \else
    \expandafter\@secondoftwo
  \fi
}
\makeatother
% Abstract states
\newcommand{\tikzastate}{e}
\newcommand{\argstate}[5]{$\tikzastate_{#1}$: $(#2, (#3, #5))$}%
\newcommand{\bmcstate}[4]{$\tikzastate_{#1}$: $(#2, (\true, #4), \{l_4\mapsto#3\})$}%
\newcommand{\indstate}[4]{$\tikzastate_{#1}$: $(#2, (\true, #4), \{l_4\mapsto#3\})$}%

%%%%%%%%%% ALGORITHMS %%%%%%%%%%
\RequirePackage[noend]{algorithmic}
\RequirePackage{algorithm}

\RequirePackage{listings}
\lstset{language=[LaTeX]TeX,columns=flexible,%
        commentstyle=\normalfont\itshape,%
        morekeywords={publishers,subtitle,maketitle,part,chapter,subsection,subsubsection,paragraph,tableofcontents,singlespacing,onehalfspacing,doublespacing,setstretch,deffootnote,marginline,columnbreak,setlength,titleref,color,textcolor,colorbox,definecolor,columncolor,rowcolor,cellcolor,@addtoreset,@removefromreset,printindex,includegraphics,listoffigures,listoftables,citet,citep,citetalias,citepalias,defcitealias,nobibliography,bibentry,shortindexingon,selectlanguage,thesection,thechapter,usetheme,usecolortheme,usefonttheme,useoutertheme,useinnertheme,setbeamertemplate,addtobeamertemplate,pause,visible,invisible,alt,alert}}
\lstdefinestyle{C}{
    language=C,
    basicstyle=\ttfamily\small,
    columns=fixed,
    morekeywords={assert},
}


%%%%%%%%%% HYPERREF %%%%%%%%%%
\RequirePackage{hyperref}
\hypersetup{colorlinks=true,urlcolor=sosyblue}
\newcommand{\mailto}[1]{\href{mailto:#1}{#1}}

%%%%%%%%%% TOOLS %%%%%%%%%%%%%%%%%%%
\newcommand\toolnamesize\smaller
\newcommand\tool[1]{{{\toolnamesize\scshape #1}\xspace}}
\newcommand\definetool[2]{\newcommand{#1}{\tool{#2}\xspace}}
\definetool{\blast}       {Blast}
\definetool{\cpachecker}  {CPAchecker}
\definetool{\cbmc}        {Cbmc}
\definetool{\cil}         {Cil}
\definetool{\llvm}        {Llvm}
\definetool{\tvla}        {Tvla}
\definetool{\ocaml}       {OCaml}
\definetool{\tvp}         {Tvp}
\definetool{\camplp}      {CamlP4}
\definetool{\foci}        {Foci}
\definetool{\tcp}         {TCP}
\definetool{\escjava}     {ESC/Java}
\definetool{\slam}        {Slam}
\definetool{\slab}        {Slab}
\definetool{\jpf}         {JPF}
\definetool{\sycmc}       {SyCMC}
\definetool{\impact}      {Impact}
\definetool{\wolverine}   {Wolverine}
\definetool{\ufo}         {Ufo}
\definetool{\java}        {Java}
\definetool{\scratch}     {Scratch}
\definetool{\mathsat}     {MathSAT5}
\definetool{\princess}    {Princess}
\definetool{\smtinterpol} {SMTInterpol}
\definetool{\zthree}      {Z3}
\definetool{\esbmc}       {Esbmc}
\definetool{\pkind}       {PKind}
\definetool{\benchexec}   {BenchExec}
\definetool{\javasmt}     {JavaSMT}
\definetool{\git}         {Git}

\definetool{\twols}       {2ls}
\definetool{\aprove}      {AProVE}
\definetool{\cascade}     {Cascade}
\definetool{\ceagle}      {Ceagle}
\definetool{\ceagleabsref}{Ceagle-Absref}
\definetool{\civl}        {Civl}
\definetool{\cpabam}      {CPA-BAM}
\definetool{\cpakind}     {CPA-kInd}
\definetool{\cparefsel}   {CPA-RefSel}
\definetool{\cpaseq}      {CPA-Seq}
\definetool{\divine}      {Divine}
\definetool{\esbmcdepthk} {Esbmc+DepthK}
\definetool{\forest}      {Forest}
\definetool{\forester}    {Forester}
\definetool{\hiprec}      {HIPrec}
\definetool{\impara}      {Impara}
\definetool{\kratos}      {Kratos}
\definetool{\lassoranker} {LassoRanker}
\definetool{\lazycseq}    {Lazy-CSeq}
\definetool{\lctd}        {LCTD}
\definetool{\llbmc}       {Llbmc}
\definetool{\lpi}         {LPI}
\definetool{\mapcheck}    {Map2Check}
\definetool{\mucseq}      {MU-CSeq}
\definetool{\pacman}      {PAC-MAN}
\definetool{\predatorhp}  {PredatorHP}
\definetool{\satabs}      {Satabs}
\definetool{\seahorn}     {SeaHorn}
\definetool{\skink}       {Skink}
\definetool{\smack}       {Smack}
\definetool{\symbiotic}   {Symbiotic}
\definetool{\symdivine}   {SymDIVINE}
\definetool{\terminator}  {Terminator}
\definetool{\uautomizer}  {Ultimate Automizer}
\definetool{\automizer}   {Automizer}
\definetool{\ukojak}      {Ultimate Kojak}
\definetool{\utaipan}     {Ultimate Taipan}
\definetool{\ulcseq}      {UL-CSeq}
\definetool{\vvt}         {Vvt}

\newcommand{\benchexecurl}{https://github.com/sosy-lab/benchexec}
\newcommand{\cpacheckerurl}{https://cpachecker.sosy-lab.org}

%%%%%%%%%% NOTATIONS %%%%%%%%%%
\newcommand{\kinduction}{\texorpdfstring{\textit{k}\protect\nobreakdash-induction}{k-induction}\xspace}
\newcommand{\kInduction}{\texorpdfstring{\textit{k}\protect\nobreakdash-Induction}{k-Induction}\xspace}
\newcommand{\CPA}{{CPA}\xspace}
\newcommand{\cegar}{{\ac{CEGAR}}\xspace}
\newcommand{\CFA}{{\ac{CFA}}\xspace}
\newcommand{\ARG}{{\ac{ARG}}\xspace}
\newcommand{\SMT}{{\ac{SMT}}\xspace}
\newcommand{\safe}{{\scshape true}\xspace}
\newcommand{\unsafe}{{\scshape false}\xspace}
\newcommand{\unknown}{{\scshape unknown}\xspace}
\newcommand{\mo}{\text{\small\scshape mo}}
\newcommand\gt{\raisebox{0.3ex}[0pt][0pt]{\smallestfontsize\ensuremath{>}}\,}
\newcommand{\true}{\mathit{true}}
\newcommand{\false}{\mathit{false}}
\newcommand{\seq}[1]{{\langle #1 \rangle}}
\newcommand{\sem}[1]{[\![ #1 ]\!]}
\newcommand{\setsem}[1]{\bigcup_{e \in #1} \sem{e}}
\newcommand{\locs}{\mathit{L}}
\newcommand{\op}{\mathit{op}}
\newcommand{\pc}{\mathit{l}}
\newcommand{\pcvar}{\mathit{l}}
\newcommand{\initial}[1]{#1_0}
\newcommand{\pci}{{\initial\pc}}
\newcommand{\pce}{{\pc_E}}
\newcommand{\target}[1]{#1_{e}}
\newcommand{\pct}{{\target\pc}}
\newcommand{\meet}{\sqcap}
\newcommand{\cpa}{\mathbb{D}}

\newcommand{\conccpa}{\mathbb{C}}
\newcommand{\loccpa}{\mathbb{L}}
\newcommand{\argcpa}{\mathbb{A}}
\newcommand{\locmcpa}{\mathbb{LM}}
\newcommand{\unrollcpa}{\mathbb{R}}
\newcommand{\boundscpa}{\mathbb{LB}}
\newcommand{\predcpa}{\mathbb{P}}
\newcommand{\explcpa}{\mathbb{E}}
\newcommand{\varcpa}{\mathbb{U}}
\newcommand{\defcpa}{\mathbb{RD}}
\newcommand{\constcpa}{\mathbb{CO}}
\newcommand{\aliascpa}{\mathbb{A}}
\newcommand{\shapecpa}{\mathbb{S}}
\newcommand{\compositecpa}{\mathbb{C}}
\newcommand{\assumptionscpa}{\mathbb{A}}
\newcommand{\overflowcpa}{\mathbb{O}}

\newcommand{\cpap}{\text{CPA\raisebox{.2ex}{+}}\xspace}
\newcommand{\cpapa}{\text{CPA\kern-1pt\raisebox{.2ex}{+\!+}}\xspace}

\newcommand{\compcpa}{\mathcal{C}}
\newcommand{\lattice}{\mathcal{E}}
\newcommand{\intlat}{\mathcal{Z}}
\newcommand{\preds}{\mathcal{P}}
\newcommand{\p}{P}

\newcommand{\Nats}{\mathbb{N}}
\newcommand{\Bools}{\mathbb{B}}
\newcommand{\Ints}{\mathbb{Z}}

\newcommand{\less}{\sqsubseteq}
\newcommand{\join}{\sqcup}
\newcommand{\sep}{\mathit{sep}}
\newcommand{\joi}{\mathit{join}}
\newcommand{\strengthen}{\mathord{\downarrow}}
\newcommand{\transconc}[1]{\smash{\stackrel{#1}{\rightarrow}}}
\newcommand{\transabs}[2]{\smash{\stackrel[#2]{#1}{\rightsquigarrow}}}
\newcommand{\mergeop}{\mathsf{merge}}
\newcommand{\merge}{\mergeop}
\newcommand{\stopop}{\mathsf{stop}}
\newcommand{\wait}{\mathsf{waitlist}}
\newcommand{\reached}{\mathsf{reached}}
\newcommand{\result}{\mathsf{result}}
\newcommand{\compare}{\preceq}
\renewcommand{\implies}{\Rightarrow}

\newcommand{\SP}[2]{{\mathsf{SP}_{#1}({#2})}}
\newcommand{\WP}[2]{{\mathsf{wp}_{#1}({#2})}}
\newcommand{\blk}{\mathsf{blk}}
\newcommand{\blknever}{\mathsf{blk}^\mathit{never}}
\newcommand{\labs}[2]{{{l^\psi}^{#1}_{\!\!#2}}}
\newcommand{\fcover}{\mathsf{fcover}}
\newcommand{\abs}[3]{(\ensuremath{{#1}})^{#2}_{#3}}
\newcommand{\boolabs}[2]{\abs{#1}{#2}{\mathbb{B}}}
\newcommand{\cartabs}[2]{\abs{#1}{#2}{\mathbb{C}}}
\newcommand{\pf}{\varphi} % path formula
\newcommand{\itp}{\tau} % interpolant
\newcommand{\af}{\tau} % abstract fact

\newcommand{\pre}{\textsf{pre}}

\newcommand{\vars}{X}
\newcommand{\ops}{Ops}
\newcommand{\astate}{e}
\newcommand{\astates}{E}
\newcommand{\PREC}{\Pi}
\newcommand{\precisions}{\Pi}
\newcommand{\programprecisions}{\locs \to 2^\precisions}
\newcommand{\pr}{\pi}
\DeclareMathOperator{\prunion}{%
%\overset{\pr}{\cup}%  Too much height
%\substack{\pr\\[-.10em]\cup}%
{\ooalign{\hidewidth$\pr$\hidewidth\cr$\bigcup$}}%
}
\newcommand{\progpr}{p}
\newcommand{\precfn}{\precop}
\newcommand{\precop}{\mathsf{prec}}
\newcommand{\assumptions}{\Psi}
\newcommand{\as}{\psi}
\newcommand{\stopAlg}{\mathit{break}}
\newcommand{\contAlg}{\mathit{continue}}

\newcommand{\dmid}{\mid}
\newcommand{\pto}{\mbox{$\;\longrightarrow\!\!\!\!\!\!\!\!\circ\;\;\;$}}
%\newcommand{\pto}{\rightharpoonup}
\newcommand{\dom}{\mathit{dom}}

\newcommand\widen{\mathsf{widen}}
%%% Symbol rather?
\newcommand{\refines}{\preccurlyeq}
\newcommand\refine{\mathsf{refine}}
\newcommand\finetune{\mathit{FineTune}}
\newcommand\extractscg{\mathit{ExtractSCG}}
\newcommand\abstr{\mathsf{abstract}}
\newcommand{\off}{\top\hspace{-2.77mm}\bot}

% Symbolicized names for ki-df, ki-ki, ki-ki-df, etc.
\newcommand{\noinject}{}
\newcommand{\injectstatic}{$\leftarrow$}
\newcommand\injectdynamic{{% hack for automatically centering the circle
    \setbox0\hbox{$\longleftarrow$}%
    \rlap{\hbox to \wd0{\hss$\,\pmb\circlearrowleft$\hss}}\box0
}}

\newcommand{\ai}{DF\noinject\xspace}
\newcommand{\kind}{KI\noinject\xspace}
\newcommand{\static}{KI\injectstatic{}DF\xspace}
\newcommand{\staticOpen}{KI\injectstatic{}\xspace}
\newcommand{\refined}{KI\injectdynamic{}}

\newcommand{\kiki}{KI\injectdynamic{}KI\xspace}
\newcommand{\kiai}{KI\injectdynamic{}DF\xspace}
\newcommand{\kikiai}{KI\injectdynamic{}KI\injectdynamic{}DF\xspace}
