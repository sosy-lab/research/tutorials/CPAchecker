#include <klee/klee.h>
extern unsigned int __VERIFIER_nondet_uint();
extern void __VERIFIER_error();

void __VERIFIER_error() { klee_assert(0); }

unsigned int __VERIFIER_nondet_uint(){
    unsigned int __sym___VERIFIER_nondet_uint;
    klee_make_symbolic(&__sym___VERIFIER_nondet_uint, sizeof(__sym___VERIFIER_nondet_uint), "__sym___VERIFIER_nondet_uint");
    return __sym___VERIFIER_nondet_uint;
}

void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: __VERIFIER_error();
  }
  return;
}

int main() {
  int n = 0;
  for (int i = 0; i<10; i++) {
    if (__VERIFIER_nondet_uint() % 2 == 0) {
      n = 1;
      break;
    };
  }
  __VERIFIER_assert(n == 1);
  return 0;
}
