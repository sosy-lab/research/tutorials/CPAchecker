# Model Checking

CPAchecker also features a variety of analyses
that fall more into the domain of Model Checking,
i.e., they rather use coverage
for determining whether a fixed point has been reached
instead of actually merging states.
These analyses tend to be more powerful, at the cost that there
is no guarantee that the analyses will terminate in general.

In the following we will have a brief look at some of the existing analyses.
A description of all analyses, or all ways to configure even a single
analysis is currently out of scope for this tutorial.

## Value Analysis {#val}

The value analysis tracks for each variable whether it can can be assigned a
definite, constant value. In cases the value of a variable is ambigous,
the value analysis will map the value of that variable to a special element
(top element) which indicates that the variable could have any possible value.
This analysis is generally fast in unrolling paths, at the cost of precision.

the value analysis can be executed like shown in the following.
For a program without a specification violation:

    scripts/cpa.sh -valueAnalysis doc/examples/example.c

For a program with a specification violation

    scripts/cpa.sh -valueAnalysis doc/examples/example_bug.c

## Predicate Analysis {#pred}

The predicate analysis performs predicate abstraction.
Infeasible counterexamples are used for discovering new predicates via interpolation
using an SMT solver (MathSat5 by default)
By default, adjustable-block encoding is used to increase
performance. The predicate analysis is a powerful analysis,
but may be slower than the value analysis in some cases:

    scripts/cpa.sh -predicateAnalysis doc/examples/example.c

### Overflow Checking {#predovf}

The predicate analysis can also be used for checking for signed integer overflows.
These are undefined behavior according to the C standard, i.e., should be avoided,
but this is often hard to spot as programmer, which means it is the ideal job
for a software verification tool such as CPAchecker.

An example of how to execute predicate analysis
for searching for overflows is as follows:

    scripts/cpa.sh -predicateAnalysis--overflow -spec config/properties/no-overflow.prp -preprocess examples/rosetta.c

## k-Induction {#kind}

CPAchecker also has an analysis that combines k-induction with an invariant generator.
The invariant generator runs in parallel to the k-induction, while the k-induction
incrementally increases the bound k.

The k-induction analsis can be executed as follows.
For a program without specification violation:

    scripts/cpa.sh -kInduction doc/examples/example.c

For a program with specification violation:

    scripts/cpa.sh -kInduction doc/examples/example_bug.c

## Symbolic Execution {#symex}

The symbolic execution analysis is similar to the value analysis.
But instead of overapproximating variable values to the top element,
it can introduce symbolic values and track constraints over these.

Execute symbolic execution like this:

    scripts/cpa.sh -symbolicExecution doc/examples/example.c

As in the other cases, here is also a command line for a program with a specification
violation:

    scripts/cpa.sh -symbolicExecution doc/examples/example_bug.c

## SMG Analysis {#smg}

The SMG analysis is tailored towards programs that make use of memory allocation and pointers.
It is usually used for checking memory safety and memory cleanup properties.

An example command line of how to execute the SMG analysis can be found here:

    scripts/cpa.sh -preprocess -smg examples/rosetta.c -spec config/properties/valid-memsafety.prp

## Termination Analysis {#term}

The termination analysis can be used to show that a program does not contain infinite executions.

An example command line can be found here:

    scripts/cpa.sh -preprocess -terminationAnalysis examples/rosetta.c -spec config/properties/termination.prp

## SV-COMP Meta-Analysis {#svcomp}

When using the prp files for specification,
you can also use the (meta-)analysis `-svompXX`
(XX refers to the year of the last SV-COMP that took place, as of writing this is 22).
This meta-analysis will select a suitable configuration for CPAchecker based on some simple characteristics
computed on the input program and on the property that shall be checked.

An example command line can be found here:

    scripts/cpa.sh -preprocess -svcomp22 examples/rosetta.c -spec config/properties/no-overflow.prp

