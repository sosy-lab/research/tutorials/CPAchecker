#include <klee/klee.h>
extern unsigned int __VERIFIER_nondet_uint();
extern void __VERIFIER_error();

void __VERIFIER_error() { klee_assert(0); }
unsigned int __VERIFIER_nondet_uint(){
    unsigned int __sym___VERIFIER_nondet_uint;
    klee_make_symbolic(&__sym___VERIFIER_nondet_uint, sizeof(__sym___VERIFIER_nondet_uint), "__sym___VERIFIER_nondet_uint");
    return __sym___VERIFIER_nondet_uint;
}

void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: __VERIFIER_error();
  }
  return;
}

int main() {
  unsigned int n = __VERIFIER_nondet_uint();
  int i = 0;
  while (n>1) {
    if (n%2==0) {
      n = n/2;
    } else {
      n = 3*n + 1;
    }
    i++;
  }
  __VERIFIER_assert(i < 6);
  return 0;
}
