#Witnesses {#witness}

## Violation Witnesses {#witnessviolation}

### Generation {#witnessviolationgeneration}

We want to generate a violation witness for the specification violation in the program
[multivar_false-unreach-call1_true-termination.c]({{book.url}}/examples/loop-acceleration/multivar_false-unreach-call1_true-termination.c):

[include](./examples/loop-acceleration/multivar_false-unreach-call1_true-termination.c)

We do this by running the following command:

    scripts/cpa.sh -predicateAnalysis \
                   -spec config/specification/sv-comp-reachability.spc \
                   -setprop counterexample.export.compressWitness=false \
                   multivar_false-unreach-call1_true-termination.c
(In case you miss sv-comp-reachability.spc, follow [this link](https://raw.githubusercontent.com/sosy-lab/sv-benchmarks/svcomp20/c/properties/unreach-call.prp) and rename).
This generates the witness in the output directory, with a name like `Counterexample.1.graphml`.
The witness file will look similar to [this]({{book.url}}/witnesses/violationwitness1.graphml).

The generated report can be found [here]({{book.url}}/reports/ViolationWitnessReport1.html).
Note that this report also contains a tab with a visual representation of the witness.
This tab is an experimental feature and thus only available if you are on the witnessreport branch of CPAchecker.

### Validation {#witnessviolationvalidation}

Now we want to validate the generated witness. For this, we call:

    scripts/cpa.sh -witnessValidation \
                   -spec config/specification/sv-comp-reachability.spc \
                   -witness output/Counterexample.1.graphml \
                   multivar_false-unreach-call1_true-termination.c

If all goes well, the output ends with something like this, indicating that the validation was successful:

    [...]
    Verification result: FALSE. Property violation (__VERIFIER_error(); called in line 6) found by chosen configuration.
    More details about the verification run can be found in the directory "./output".
    Graphical representation included in the file "./output/Counterexample.1.html".

`FALSE` is the correct result because we are dealing with a violation witness.
The validator was able to reproduce the violation and thus came to the right conclusion (`FALSE`).

## Correctness Witnesses {#witnesscorrectness}

### Generation {#witnesscorrectnessgeneration}

We want to generate a correctness witness for the the program
[multivar_true-unreach-call1_true-termination.c]({{book.url}}/examples/loop-acceleration/multivar_true-unreach-call1_true-termination.c):

[include](./examples/loop-acceleration/multivar_true-unreach-call1_true-termination.c)

We do this by running the following command:

```bash
    scripts/cpa.sh -predicateAnalysis \
                   -spec config/specification/sv-comp-reachability.spc \
                   -setprop cpa.arg.proofWitness=proofWitness.graphml \
                   -setprop cpa.arg.compressWitness=false \
                   multivar_true-unreach-call1_true-termination.c
```

This generates the witness in the output directory, with the name we specified on the command line: `proofWitness.graphml`.
The witness file will look similar to [this]({{book.url}}/witnesses/proofwitness1.graphml).

The generated report can be found [here]({{book.url}}/reports/CorrectnessWitnessReport1.html).
Note that this report also contains a tab with a visual representation
of the witness that also shows the calculated invariant when hovering above the blue node.
This tab is an experimental feature and thus only available if you are on the witnessreport branch of CPAchecker. 

### Validation {#witnesscorrectnessvalidation}

Now we want to validate the generated witness. For this, we call (like in the case of violation witnesses):

```bash
    scripts/cpa.sh -witnessValidation \
                   -spec config/specification/sv-comp-reachability.spc \
                   -witness output/proofWitness.graphml \
                   multivar_true-unreach-call1_true-termination.c
```

Here, the validation is successful if the end of the output looks like this:

    Verification result: TRUE. No property violation found by chosen configuration.
    More details about the verification run can be found in the directory "./output".
    Graphical representation included in the file "./output/Report.html".

The validator was able to arrive at a proof of correctness using the information in the witness
(e.g. the witness can provide the invariants that are vital to the proof and are otherwise hard to calculate).

For simple programs like the one used here, the validator will arrive at the same conclusion even if no/an empty witness is supplied.
You can try this yourself if you omit the option `-witness output/proofWitness.graphml` or remove the invariant from the witness file.
For the witness file that is linked here, the invariant is visible in lines 51-52, shown here:

[include:51-52,unindent:"true",template:"default"](witnesses/proofwitness1.graphml)

Just remove these lines to remove the invariant from the witness. This would also work in the other direction,
you could manually add (better) invariants to a witness and observe whether this improves the validation of the witness.

For more challenging problems however, the validator will generally not be able to generate a proof on its own.
We will look at an example for this in the section [Witnesses and Invariants](#witnessinvariant).

## Witnesses and Invariants {#witnessinvariant}

### A first Example {#witnessinvariantfirst}

We will have a look at the program [even.c]({{book.url}}/examples/even.c):

[include](./examples/even.c)

This program is correct because the loop preserves the invariant `x % 2 == 0`. Therefore the assertion after the loop will always hold.
k-induction in its default setting, which is used for the witness validation, cannot derive this invariant,
so the following call will not terminate:

    scripts/cpa.sh -kInduction \
                   -spec config/specification/sv-comp-reachability.spc \
                   -setprop cpa.arg.proofWitness=proofWitness.graphml \
                   -setprop cpa.arg.compressWitness=false \
                   even.c

Because of this, an additional option has been added that allows k-induction to deal with invariants that contain modulo-two operations:

    -setprop cpa.invariants.useMod2Template=true

When we run CPAchecker with this additional option,
we will get a [report]({{book.url}}/reports/CorrectnessWitnessReport2.html)
and a [witness]({{book.url}}/witnesses/proofwitness2.graphml) containing the necessary invariant `x % 2 == 0`.

While the handling of invariants that involve modulo-two operations is not hard to implement, it would be nicer if the analysis finds the invariants without further assistance.
This can be seen by the fact that we will fail to find the right invariant this way if it involves a modulo-four operation instead of a modulo-two operation.
The corresponding program [mod4.c]({{book.url}}/examples/mod4.c) looks like this:

[include](./examples/mod4.c)

In cases like this, PDR is often good in finding the neccessary invariants.
We can use it as invariant generator for k-induction like this:

    scripts/cpa.sh -kInduction-kipdrInvariants \
                   -spec config/specification/sv-comp-reachability.spc \
                   -setprop cpa.arg.proofWitness=proofWitness.graphml \
                   -setprop cpa.arg.compressWitness=false \
                   mod4.c

This will successfully prove the program correct and give us a [report]({{book.url}}/reports/CorrectnessWitnessReport3-pdr.html) and [witness]({{book.url}}/witnesses/proofwitness3-pdr.graphml).

### Excercise: Manually adding Invariants 1 {#witnessinvariantex1}
The witness that we got this way does not contain the right invariants that would allow k-induction on its own to validate it.

- Try to validate the witness without the invariants, does this fail as promised?
- What is the loop invariant that is necessary to prove the program correct?
- Can you add this invariant into this witness?
- Will k-induction be able to validate the witness with the invariant(s) that you added?

### Excercise: Manually adding Invariants 2 {#witnessinvariantex2}
Now we look at a different example named [eq2.c]({{book.url}}/examples/eq2.c), where the invariant is more complicated:

[include](./examples/eq2.c)

With PDR we again get a witness, but it lacks the invariants needed for k-induction to validate the witness.
You can look at the [witness]({{book.url}}/witnesses/proofwitness4.graphml)) in this [report]({{book.url}}/reports/CorrectnessWitnessReport4.html).

- Will the validation of the witness succeed?
- What is the loop invariant that is necessary to prove the program correct?
- Can you add this invariant into this witness?
- Will the validation now succeed?


## Useful Links {#witnesslinks}

- [Violation-Witness Publication](https://www.sosy-lab.org/research/verification-witnesses/)
- [Correctness-Witness Publication](https://www.sosy-lab.org/research/correctness-witnesses/)
- [Most recent syntax for witness generation/validation](https://github.com/sosy-lab/sv-witnesses)
