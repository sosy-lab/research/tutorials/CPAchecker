#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <assert.h>

#define LIMIT 1000

/* Instrumentation Challenge
 * The goal here is to change this program such that
 * you can verifiy different properties of the program
 * with a verifier like CPAchecker.
 * Things that you can check:
 * 1. do the asserts in the program hold?
 * 2. can you change an assertion or the program such that the verifier detects the violation?
 * 3. is it free from unsigned integer overflows? (undefined behavior)
 * 4. are there any memory safety violations?
 * 5. will the program always terminate after a finite amount of user interactions?
 */

//TODO: declare functions relevant for verification here

int main() {
    //TODO: tell the verifier which non-deterministic values the variable "secret" can contain
    srand(time(NULL));
    int secret = rand() % LIMIT;

    int choice = -1;
    int lower = 0;
    int upper = LIMIT;

    printf("Welcome, please guess my number from 0 to %d \n", LIMIT-1);
    do {
      while (choice >= upper || choice <= lower) {
        printf("Enter an integer: \n");
        //TODO: tell the verifier what effect scanf will have on the value of the variable "choice"
        scanf("%d", &choice);
      }
      if (choice>secret) {
          printf("Too high!");
          upper = choice;
      } else if (choice < secret) {
          printf("Too low!");
          lower = choice;
      }
    } while (choice != secret);
    //TODO: find out how to tell the verifier to check for these assertions.
    assert(choice == secret);
    assert(choice < upper);
    assert(choice > lower);
    printf("You guessed correctly!\n");
    return 0;
}
