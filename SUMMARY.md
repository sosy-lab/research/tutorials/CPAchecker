# Summary
* [Introduction](README.md)
* [Installation](Installation.md)
    * [CPAchecker](Installation.md#instcpa)
        * [Windows](Installation.md#instcpawin)
        * [Linux](Installation.md#instcpalinux)
* [Short introduction to CPAchecker](ShortIntroductionCPAchecker.md)
    * [Requirements](ShortIntroductionCPAchecker.md#requirements)
    * [First run](ShortIntroductionCPAchecker.md#firstrun)
        * [Result](ShortIntroductionCPAchecker.md#result)
        * [Example 1: Correctness Report](ShortIntroductionCPAchecker.md#example1)
        * [Example 2: Counterexample Report](ShortIntroductionCPAchecker.md#example2)
* [CPAchecker's HTML Report](HTMLReport.md)
    * [Correctness Report](HTMLReport.md#htmlcorrectreport)
        * [Generation](HTMLReport.md#htmlcorrectreportgeneration)
        * [Features](HTMLReport.md#htmlcorrectreportfeatures)
    * [Counterexample Report](HTMLReport.md#htmlcexreport)
        * [Generation](HTMLReport.md#htmlcexreportgeneration)
        * [Features](HTMLReport.md#htmlcexreportfeatures)
* [Data-Flow Analysis](DataFlowAnalysis.md)
    * [Value Analysis](DataFlowAnalysis.md#va)
    * [Reaching-Definitions Analysis](DataFlowAnalysis.md#rda)
    * [Interval Analysis](DataFlowAnalysis.md#ia)
    * [Sign Analysis](DataFlowAnalysis.md#sa)
* [Specification/Instrumentation](Specification.md)
    * [Specification Automata](Specification.md#specaut)
    * [SV-COMP Properties](Specification.md#prp)
    * [Instrumentation](Specification.md#inst)
    * [Example](Specification.md#ex)
* [Model Checking](ModelChecking.md)
    * [Value Analysis](ModelChecking.md#val)
    * [Predicate Analysis](ModelChecking.md#pred)
    * [Overflow Checking](ModelChecking.md#kind)
    * [k-Induction](ModelChecking.md#kind)
    * [Symbolic Execution](ModelChecking.md#symex)
    * [Termination Analysis](ModelChecking.md#term)
    * [SV-COMP Meta-Analysis](ModelChecking.md#svcomp)
* [Witnesses](Witnesses.md)
    * [Violation Witnesses](Witnesses.md#witnessviolation)
        * [Generation](Witnesses.md#witnessviolationgeneration)
        * [Validation](Witnesses.md#witnessviolationvalidation)
    * [Correctness Witnesses](Witnesses.md#witnesscorrectness)
        * [Generation](Witnesses.md#witnesscorrectnessgeneration)
        * [Validation](Witnesses.md#witnesscorrectnessvalidation)
    * [Witnesses and Invariants](Witnesses.md#witnessinvariant)
        * [A first Example](Witnesses.md#witnessinvariantfirst)
        * [Exercise 1](Witnesses.md#witnessinvariantex1)
        * [Exercise 2](Witnesses.md#witnessinvariantex2)
    * [Useful Links](Witnesses.md#witnesslinks)
* [Combining Verifiers](CombiningVerifiers.md)
  * [Conditional Model Checking](CombiningVerifiers.md#cmc)
    * [Example](CombiningVerifiers.md#cmcex1)
  * [Reducer-Based Construction of Conditional Verifiers](CombiningVerifiers.md#reducerbased)
    * [Example](CombiningVerifiers.md#reducerbasedex1)
  * [Useful Links](CombiningVerifiers.md#links)
* [Automatic Testing](Installation-Testers.md)
  * [Klee](Installation-Testers.md#instklee)
  * [TBF](Installation-Testers.md#insttbf)
  * [AFL](Installation-Testers.md#instafl)
  * [Finding Bugs](FindingBugs.md)
    * [With CPAchecker](FindingBugs.md#fbcpa)
    * [With Klee](FindingBugs.md#fbklee)
    * [Another Example](FindingBugs.md#fbex3)
* [Projects](Projects.md)
    * [Instrumentation Challenge](Guesser.md)
        * [The Game](Guesser.md#game)
        * [Program with Challenge](Guesser.md#prog)
    * [Traffic-Light System](TrafficLights.md)
        * [Introduction](TrafficLights.md#introduction)
        * [Requirements](TrafficLights.md#requirements)
        * [System Design](TrafficLights.md#design)
        * [System Implementation](TrafficLights.md#implementation)
        * [Specification](TrafficLights.md#specs)
        * [Verification](TrafficLights.md#verification)
            * [Using CPAchecker](TrafficLights.md#cpachecker)
            * [Using Ultimate Automizer](TrafficLights.md#uautomizer)
