extern unsigned int __VERIFIER_nondet_uint();
extern void __VERIFIER_error();

void __VERIFIER_assert(int cond) {
  if (!(cond)) {
    ERROR: __VERIFIER_error();
  }
  return;
}

int main() {
  unsigned int n = __VERIFIER_nondet_uint();
  int i = 0;
  while (n>1) {
    if (n%2==0) {
      n = n/2;
    } else {
      n = 3*n + 1;
    }
    i++;
  }
  __VERIFIER_assert(i < 6);
  return 0;
}
